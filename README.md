# matlib

## Building

This project uses CMake. To build, run the following commands.

```bash
sudo apt-get install cmake build-essential
mkdir build
cmake -S . -B build
cmake --build build
```

## Examples

Building the project with `BUILD_MATLIB_EXAMPLES` set to `ON` will build the library's examples.

### transformation

This is a brief example application for *matlib*. The example involves using matrices for representing transformations and using the library's functionality to apply these transformations. See [the examples folder](https://gitlab.com/josephagrane/matlib/-/tree/main/examples) for examples on how to use the library. The [docs page](https://josephagrane.gitlab.io/matlib/) also provides a technical reference.
