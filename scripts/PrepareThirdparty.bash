#!/bin/bash
# matlib thirdparty get/build script.
# Copyright (C) 2023-2023 Joseph Agrane.
# This file is part of matlib.
# matlib is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
# matlib is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# You should have received a copy of the GNU General Public License
# along with matlib; see the file COPYING. If not see
# <http://www.gnu.org/licenses/>.

THIRDPARTY_DIR="thirdparty"
GTEST_DIR="${THIRDPARTY_DIR}/googletest-release-1.12.1"
GTEST_BUILD_DIR="${GTEST_DIR}/build"
GTEST_ARCHIVE_NAME="gtest.tar.gz"
GTEST_URL="https://github.com/google/googletest/archive/refs/tags/release-1.12.1.tar.gz"

# the downloaded libraries will reside here
mkdir -p "${THIRDPARTY_DIR}"
# this command gets googletest and puts it in thirdparty
wget "${GTEST_URL}" -O "${THIRDPARTY_DIR}/${GTEST_ARCHIVE_NAME}"
# extract googletest
tar -xv -f "${THIRDPARTY_DIR}/${GTEST_ARCHIVE_NAME}" -C "${THIRDPARTY_DIR}"
# cmake commands to build googletest
mkdir "${GTEST_BUILD_DIR}"
cmake -S "${GTEST_DIR}" -B "${GTEST_BUILD_DIR}"
cmake --build "${GTEST_BUILD_DIR}"

unset THIRDPARTY_DIR GTEST_DIR GTEST_ARCHIVE_NAME GTEST_URL GTEST_BUILD_DIR