#include <matlib/matlib.h>
#include <math.h>
#include <stdio.h>

#define RAD(x) x * (3.14159265358979323846 / 180.0)

struct transform
{
    matrix_t rotation;      // 3x3
    matrix_t translation;   // 3x1
};

typedef struct transform transform_t;

void initialize_transform(transform_t* transform);

void free_transform(transform_t* transform);

void set_translation(
    transform_t* transform,
    double x, double y, double z);

void set_rotation(transform_t* transform,
    double roll, double pitch, double yaw);

void apply_transform(
    transform_t* transform,
    const transform_t * const lhs,
    const transform_t * const rhs);

/**
 * Implementations here
*/

void initialize_transform(transform_t* transform) {
    // initialize the matrices
    matlib_matrix_create(&transform->rotation, MATLIB_FLOAT64, 3, 3);
    matlib_matrix_create(&transform->translation, MATLIB_FLOAT64, 3, 1);
}

void free_transform(transform_t* transform) {
    // free up matrix data
    matlib_matrix_free(&transform->rotation);
    matlib_matrix_free(&transform->translation);
}

void set_translation(
    transform_t* transform,
    double x, double y, double z) {
        // cast the data to the type we
        // defined it to be
        double (*translation)[1] = transform->translation.data;  // 3x1 multidim
        // indices are [row][column]
        translation[0][0] = x;
        translation[1][0] = y;
        translation[2][0] = z;
}

// apply rotation in ZYX order
void set_rotation(transform_t* transform,
    double roll, double pitch, double yaw)
{
    matrix_t
        rotationMatrixX,
        rotationMatrixY,
        rotationMatrixZ;

    matlib_matrix_create(
        &rotationMatrixX,
        MATLIB_FLOAT64,
        3, 3);
    matlib_matrix_create(
        &rotationMatrixY,
        MATLIB_FLOAT64,
        3, 3);
    matlib_matrix_create(
        &rotationMatrixZ,
        MATLIB_FLOAT64,
        3, 3);

    double
        (*rotationX)[3] = rotationMatrixX.data,
        (*rotationY)[3] = rotationMatrixY.data,
        (*rotationZ)[3] = rotationMatrixZ.data;

    // fill data with zero
    double zero = 0.0;
    matlib_matrix_fill(&rotationMatrixX, &zero);
    matlib_matrix_fill(&rotationMatrixY, &zero);
    matlib_matrix_fill(&rotationMatrixZ, &zero);

    // https://en.wikipedia.org/wiki/Rotation_matrix

    rotationX[0][0] = 1.0;
    rotationX[1][1] = cos(roll);
    rotationX[1][2] = -sin(roll);
    rotationX[2][1] = sin(roll);
    rotationX[2][2] = cos(roll);

    rotationY[0][0] = cos(pitch);
    rotationY[0][2] = sin(pitch);
    rotationY[1][1] = 1.0;
    rotationY[2][0] = -sin(pitch);
    rotationY[2][2] = cos(pitch);

    rotationZ[0][0] = cos(yaw);
    rotationZ[0][1] = -sin(yaw);
    rotationZ[1][0] = sin(yaw);
    rotationZ[1][1] = cos(yaw);
    rotationZ[2][2] = 1.0;

    // set result to zeroes
    matlib_matrix_fill(&transform->rotation, &zero);
    matlib_matrix_mul(&transform->rotation, &rotationMatrixY, &rotationMatrixZ);
    matlib_matrix_mul(&transform->rotation, &rotationMatrixX, &transform->rotation);

    matlib_matrix_free(&rotationMatrixX);
    matlib_matrix_free(&rotationMatrixY);
    matlib_matrix_free(&rotationMatrixZ);
}

void printMatrix(const matrix_t * const m)
{
    double (*array) = m->data;
    for(size_t i = 0; i < m->width * m->height; i++)
    {
        if(i != 0 && i % m->width == 0)
            printf("\n");
        printf("%f ", array[i]);
    }
    printf("\n");
}

int main()
{

    transform_t transform;
    initialize_transform(&transform);

    set_translation(
        &transform,
        1.5, -3.5, 4.5);

    set_rotation(
        &transform,
        RAD(45),
        RAD(90),
        RAD(60));

    matrix_t a, b;
    double zero = 0.0;

    matlib_matrix_create(
        &a,
        MATLIB_FLOAT64,
        3, 1);
    matlib_matrix_create(
        &b,
        MATLIB_FLOAT64,
        3, 1);
    double (*pos)[1] = a.data;
    pos[0][0] = 5.0;
    pos[1][0] = -2.0;
    pos[2][0] = 3.0;
    matlib_matrix_fill(&b, &zero);

    printf("Translation: \n");
    printMatrix(&transform.translation);
    printf("Rotation: \n");
    printMatrix(&transform.rotation);

    printf("Position (Before Transformation): \n");
    printMatrix(&a);

    matlib_matrix_mul(&b, &transform.rotation, &a);  // b = (rotation x a) + translation
    matlib_matrix_add(&b, &b, &transform.translation);

    printf("Position (After Transformation): \n");
    printMatrix(&b);

    free_transform(&transform);

    return 0;
}
