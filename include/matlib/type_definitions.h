/* matlib type definitions and macros.
   Copyright (C) 2023-2023 Joseph Agrane.
This file is part of matlib.
matlib is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation; either version 3, or (at your option) any later
version.
matlib is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.
You should have received a copy of the GNU General Public License
along with matlib; see the file COPYING. If not see
<http://www.gnu.org/licenses/>. */
/** \file type_definitions.h */
#ifndef INCLUDE_MATLIB_TYPE_DEFINITIONS_H_
#define INCLUDE_MATLIB_TYPE_DEFINITIONS_H_

#include <stdint.h>

/** uint8_t */
#define MATLIB_UINT8    (uint8_t)0
/** uint16_t */
#define MATLIB_UINT16   (uint8_t)1
/** uint32_t */
#define MATLIB_UINT32   (uint8_t)2
/** uint64_t */
#define MATLIB_UINT64   (uint8_t)3

/** int8_t */
#define MATLIB_INT8     (uint8_t)4
/** int16_t */
#define MATLIB_INT16    (uint8_t)5
/** int32_t */
#define MATLIB_INT32    (uint8_t)6
/** int64_t */
#define MATLIB_INT64    (uint8_t)7

/** float */
#define MATLIB_FLOAT32  (uint8_t)8
/** double */
#define MATLIB_FLOAT64  (uint8_t)9

/** Performs some operation on a number stored at
 *  memory pointed to by void*.
 */
#define MATLIB_UNKOWN_OP(result, lhs, rhs, op, type) \
    switch (type) \
    { \
        case MATLIB_UINT8: \
            *((uint8_t*)(result)) = *((uint8_t*)(lhs)) op *((uint8_t*)(rhs)); \
            break; \
        case MATLIB_UINT16: \
            *((uint16_t*)(result)) = \
                *((uint16_t*)(lhs)) op *((uint16_t*)(rhs)); \
            break; \
        case MATLIB_UINT32: \
            *((uint32_t*)(result)) = \
                *((uint32_t*)(lhs)) op *((uint32_t*)(rhs)); \
            break; \
        case MATLIB_UINT64: \
            *((uint64_t*)(result)) = \
                *((uint64_t*)(lhs)) op *((uint64_t*)(rhs)); \
            break; \
        case MATLIB_INT8: \
            *((int8_t*)(result)) = *((int8_t*)(lhs)) op *((int8_t*)(rhs)); \
            break; \
        case MATLIB_INT16: \
            *((int16_t*)(result)) = *((int16_t*)(lhs)) op *((int16_t*)(rhs)); \
            break; \
        case MATLIB_INT32: \
            *((int32_t*)(result)) = *((int32_t*)(lhs)) op *((int32_t*)(rhs)); \
            break; \
        case MATLIB_INT64: \
            *((int64_t*)(result)) = *((int64_t*)(lhs)) op *((int64_t*)(rhs)); \
            break; \
        case MATLIB_FLOAT32: \
            *((float*)(result)) = *((float*)(lhs)) op *((float*)(rhs)); \
            break; \
        case MATLIB_FLOAT64: \
            *((double*)(result)) = *((double*)(lhs)) op *((double*)(rhs)); \
            break; \
    }

/** Returns the size of the corresponding type. */
#define MATLIB_DTYPE_SIZE(result, dtype) \
    switch (dtype) \
    { \
        case MATLIB_UINT8: \
            result = sizeof(uint8_t); \
            break; \
        case MATLIB_UINT16: \
            result = sizeof(uint16_t); \
            break; \
        case MATLIB_UINT32: \
            result = sizeof(uint32_t); \
            break; \
        case MATLIB_UINT64: \
            result = sizeof(uint64_t); \
            break; \
        case MATLIB_INT8: \
            result = sizeof(int8_t); \
            break; \
        case MATLIB_INT16: \
            result = sizeof(int16_t); \
            break; \
        case MATLIB_INT32: \
            result = sizeof(int32_t); \
            break; \
        case MATLIB_INT64: \
            result = sizeof(int64_t); \
            break; \
        case MATLIB_FLOAT32: \
            result = sizeof(float); \
            break; \
        case MATLIB_FLOAT64: \
            result = sizeof(double); \
            break; \
    }

/** Casts some element pointed to by
 * void* to type.
 */
#define MATLIB_TYPECAST(pDest, value, type) \
    switch (type) \
    { \
        case(MATLIB_UINT8): \
            *((uint8_t*)(pDest)) = (uint8_t)(value); \
            break; \
        case(MATLIB_UINT16): \
            *((uint16_t*)(pDest)) = (uint16_t)(value); \
            break; \
        case(MATLIB_UINT32): \
            *((uint32_t*)(pDest)) = (uint32_t)(value); \
            break; \
        case(MATLIB_UINT64): \
            *((uint64_t*)(pDest)) = (uint64_t)(value); \
            break; \
        case(MATLIB_INT8): \
            *((int8_t*)(pDest)) = (int8_t)(value); \
            break; \
        case(MATLIB_INT16): \
            *((int16_t*)(pDest)) = (int16_t)(value); \
            break; \
        case(MATLIB_INT32): \
            *((int32_t*)(pDest)) = (int32_t)(value); \
            break; \
        case(MATLIB_INT64): \
            *((int64_t*)(pDest)) = (int64_t)(value); \
            break; \
        case(MATLIB_FLOAT32): \
            *((float*)(pDest)) = (float)(value); \
            break; \
        case(MATLIB_FLOAT64): \
            *((double*)(pDest)) = (double)(value); \
            break; \
    }

#endif  // INCLUDE_MATLIB_TYPE_DEFINITIONS_H_
