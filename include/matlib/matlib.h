/* matlib structure and functions definitions header.
   Copyright (C) 2023-2023 Joseph Agrane.
This file is part of matlib.
matlib is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation; either version 3, or (at your option) any later
version.
matlib is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.
You should have received a copy of the GNU General Public License
along with matlib; see the file COPYING. If not see
<http://www.gnu.org/licenses/>. */
/** \file matlib.h */
#ifndef INCLUDE_MATLIB_MATLIB_H_
#define INCLUDE_MATLIB_MATLIB_H_

#include <matlib/type_definitions.h>
#include <matlib/error_definitions.h>

#include <stdint.h>
#include <stddef.h>

/**
 * \brief A structure representing a matrix.
*/
struct matrix {
    /** The number of columns in the matrix. */
    size_t width;
    /** The number of rows in the matrix. */
    size_t height;
    /**
    * A number representing the data type stored withing the matrix.
    * Must be one of the following:
    *  - \ref MATLIB_INT8
    *  - \ref MATLIB_INT16
    *  - \ref MATLIB_INT32
    *  - \ref MATLIB_INT64
    *  - \ref MATLIB_UINT8
    *  - \ref MATLIB_UINT16
    *  - \ref MATLIB_UINT32
    *  - \ref MATLIB_UINT64
    *  - \ref MATLIB_FLOAT32
    *  - \ref MATLIB_FLOAT64
    */
    uint8_t dtype;
    /** The size, in bytes, of the datatype. */
    size_t elemSize;
    /** Row-major data storage. */
    void *data;
};

typedef struct matrix matrix_t;

/**
 * Initializes a \ref matrix_t struct.
 * \param[out] pMatrix The matrix to initialize
 * \param[in] dtype The datatype to store within the
 * matrix. Can be either:
 *  - \ref MATLIB_INT8
 *  - \ref MATLIB_INT16
 *  - \ref MATLIB_INT32
 *  - \ref MATLIB_INT64
 *  - \ref MATLIB_UINT8
 *  - \ref MATLIB_UINT16
 *  - \ref MATLIB_UINT32
 *  - \ref MATLIB_UINT64
 *  - \ref MATLIB_FLOAT32
 *  - \ref MATLIB_FLOAT64
 * \param[in] height The number of rows to set up in the matrix.
 * \param[in] width The number of columns to set up in the matrix.
 * \return Error Code:
 *  - \ref MATLIB_STATUS_OK
*/
int matlib_matrix_create(
    matrix_t *pMatrix,
    uint8_t dtype,
    size_t height, size_t width
);

/**
 * Frees the heap data of a matrix.
 * \param pMatrix The matrix to free. The value
 * pointed to by pMatrix is no longer valid after
 * this function call.
 * Does not call free() on pMatrix.
 * \return Error Code:
 *  - \ref MATLIB_STATUS_OK
*/
int matlib_matrix_free(matrix_t *pMatrix);

/**
 * Fills all the elements of pMatrix with the
 * value pointed to by value.
 * \param pMatrix The matrix whose data will be filled.
 * \param value A pointer to the value to fill pMatrix with.
 * This should be of the type represented by \ref matrix_t.dtype
 * and should point to data which is \ref matrix_t.elemSize bytes
 * wide.
 * \return Error Code:
 *  - \ref MATLIB_STATUS_OK
 *  - \ref MATLIB_STATUS_UNINITIALIZED_MATRIX
*/
int matlib_matrix_fill(matrix_t *pMatrix, void *value);

/**
 * Copies a \ref matrix_t struct.
 * \param[in] src The matrix to copy from.
 * \param[out] dest The matrix to copy to. Must be
 * initialized prior to the function call.
 * \return Error Code:
 *  - \ref MATLIB_STATUS_OK
 *  - \ref MATLIB_STATUS_UNINITIALIZED_MATRIX
 *  - \ref MATLIB_STATUS_INCOMPATIBLE_DTYPE
 *  - \ref MATLIB_STATUS_INCOMPATIBLE_SHAPE
*/
int matlib_matrix_copy(const matrix_t * const src, matrix_t* dest);

/**
 * Performs a transpose operation.
 * \param pMat A pointer to the matrix to transpose.
 * \return Error Code:
 *  - \ref MATLIB_STATUS_OK
 *  - \ref MATLIB_STATUS_UNINITIALIZED_MATRIX
*/
int matlib_matrix_transpose(matrix_t * pMat);

/**
 * Calculates the determinant of a matrix.
 * Doesn't work correctly for non-floating point
 * matrices.
 * \param[in] pMatrix The matrix whose
 * determinant will be calculated.
 * \param[out] pDeterminant A pointer to a block
 * of memory \ref matrix_t.elemSize bytes wide.
 * The output value will be of the type represented
 * by \ref matrix_t.dtype.
 * \return Error Code:
 *  - \ref MATLIB_STATUS_OK
 *  - \ref MATLIB_STATUS_UNINITIALIZED_MATRIX
 *  - \ref MATLIB_STATUS_INCOMPATIBLE_SHAPE
*/
int matlib_matrix_determinant(
    const matrix_t * const pMatrix,
    void * pDeterminant);

/**
 * Calculates the inverse of a matrix.
 * Doesn't work correctly for non-floating point
 * matrices.
 * \param[out] pResult The matrix to make equal
 * to the determinant of pMatrix. Must be initialized before
 * the function call. Must not be equal to pMatrix.
 * \param[in] pMatrix The matrix to invert.
 * \return Error Code:
 *  - \ref MATLIB_STATUS_OK
 *  - \ref MATLIB_STATUS_UNINITIALIZED_MATRIX
 *  - \ref MATLIB_STATUS_INCOMPATIBLE_DTYPE
 *  - \ref MATLIB_STATUS_INCOMPATIBLE_SHAPE
*/
int matlib_matrix_inverse(matrix_t * pResult, const matrix_t * const pMatrix);

/**
 * Performs matrix multiplication on two matrices.
 * \param[out] pResult The result of the multiplication.
 * Must be initialized prior to the function call.
 * \param[in] pLhs The matrix on the left hand side of the multiplication.
 * \param[in] pRhs The matrix on the right hand side of the multiplication.
 * \return Error Code:
 *  - \ref MATLIB_STATUS_OK
 *  - \ref MATLIB_STATUS_UNINITIALIZED_MATRIX
 *  - \ref MATLIB_STATUS_INCOMPATIBLE_DTYPE
 *  - \ref MATLIB_STATUS_INCOMPATIBLE_SHAPE
*/
int matlib_matrix_mul(
    matrix_t* pResult,
    const matrix_t * const pLhs,
    const matrix_t * const pRhs);

/**
 * Performs matrix addition on two matrices.
 * \param[out] pResult The result of the addition. Must be
 * initialized prior to the function call.
 * \param[in] pLhs The matrix on the left-hand side of the addition.
 * \param[in] pRhs The matrix on the right-hand side of the addition.
 * \return Error Code:
 *  - \ref MATLIB_STATUS_OK
 *  - \ref MATLIB_STATUS_UNINITIALIZED_MATRIX
 *  - \ref MATLIB_STATUS_INCOMPATIBLE_DTYPE
 *  - \ref MATLIB_STATUS_INCOMPATIBLE_SHAPE
*/
int matlib_matrix_add(
    matrix_t* pResult,
    const matrix_t * const pLhs,
    const matrix_t * const pRhs);

/**
 * Performs matrix subtraction on two matrices.
 * \param[out] pResult The result of the subtraction. Must be
 * initialized prior to the function call.
 * \param[in] pLhs The matrix on the left-hand-side of the subtraction.
 * \param[in] pRhs The matrix on the right-hand-side of the subtraction.
 * \return Error Code:
 *  - \ref MATLIB_STATUS_OK
 *  - \ref MATLIB_STATUS_UNINITIALIZED_MATRIX
 *  - \ref MATLIB_STATUS_INCOMPATIBLE_DTYPE
 *  - \ref MATLIB_STATUS_INCOMPATIBLE_SHAPE
*/
int matlib_matrix_sub(
    matrix_t* pResult,
    const matrix_t * const pLhs,
    const matrix_t * const pRhs);

/**
 * Multiplies all the elements of a matrix by a scalar.
 * \param[out] pResult The result of the multiplication
 * and the matrix to be multiplied.
 * Must be initialized prior to the function call.
 * \param[in] pScalar The number which all elements of pResult
 * will be multipled by. Must point to data which is \ref matrix_t.elemSize
 * bytes wide and of the type represented by \ref matrix_t.dtype.
 * \return Error Code:
 *  - \ref MATLIB_STATUS_OK
 *  - \ref MATLIB_STATUS_UNINITIALIZED_MATRIX
*/
int matlib_scalar_mul(matrix_t* pResult, const void * const pScalar);

/**
 * Divides all the elements of a matrix by a scalar.
 * \param[out] pResult The result of the division and the matrix
 * to be divided.
 * Must be initialized prior to the function call.
 * \param[in] pScalar The number which all elements of pResult
 * will be divided by. Must point to data which is \ref matrix_t.elemSize
 * bytes wide and the type represented by \ref matrix_t.dtype.
 * \return Error Code:
 *  - \ref MATLIB_STATUS_OK
 *  - \ref MATLIB_STATUS_UNINITIALIZED_MATRIX
*/
int matlib_scalar_div(matrix_t* pResult, const void * const pScalar);

/**
 * Returns a sub-matrix generated by removing a row and column
 * from the input matrix.
 * \param[out] pResult The resulting sub-matrix. Must be
 * initialized prior to the function call. Must not be equal
 * to pMatrix.
 * \param[in] pMatrix The matrix to generate the sub-matrix from.
 * \param[in] coveredRow The index of the row to exclude from the sub-matrix.
 * \param[in] coveredCol The index of the column to exclude from the sub-matrix.
 * \return Error Code:
 *  - \ref MATLIB_STATUS_OK
 *  - \ref MATLIB_STATUS_UNINITIALIZED_MATRIX
 *  - \ref MATLIB_STATUS_INCOMPATIBLE_DTYPE
 *  - \ref MATLIB_STATUS_INCOMPATIBLE_SHAPE
 *  - \ref MATLIB_STATUS_INDEX_OUT_OF_BOUNDS
*/
int matlib_cover_column_row(
    matrix_t* pResult,
    const matrix_t * const pMatrix,
    size_t coveredRow, size_t coveredCol);

/**
 * Returns the matrix of cofactors.
 * \param[out] pResult The output matrix which will become
 * the matrix of cofactors of pMatrix. Must be initialized
 * prior to the function call. Must not be equal to pMatrix.
 * \param[in] pMatrix The matrix whose matrix of cofactors
 * will be calculated.
 * \return Error Code:
 *  - \ref MATLIB_STATUS_OK
 *  - \ref MATLIB_STATUS_UNINITIALIZED_MATRIX
 *  - \ref MATLIB_STATUS_INCOMPATIBLE_DTYPE
 *  - \ref MATLIB_STATUS_INCOMPATIBLE_SHAPE
*/
int matlib_matrix_of_cofactors(
    matrix_t* pResult,
    const matrix_t * const pMatrix);

/**
 * Gets the column of a matrix.
 * \param[in] pMat The matrix from whic to extract the column.
 * \param[in] col The index of the column to extract.
 * \param[out] dest The memory location to copy the column to.
 * Must be at least \ref matrix_t.height * \ref matrix_t.elemSize
 * long in bytes. The elements copied into dest will be of
 * the type represented by \ref matrix_t.dtype each \ref matrix_t.elemSize
 * bytes wide.
 * \return Error Code:
 *  - \ref MATLIB_STATUS_OK
 *  - \ref MATLIB_STATUS_UNINITIALIZED_MATRIX
 *  - \ref MATLIB_STATUS_INDEX_OUT_OF_BOUNDS
*/
int matlib_get_column(const matrix_t* const pMat, size_t col, void* dest);

#endif  // INCLUDE_MATLIB_MATLIB_H_
