/* matlib error definitions.
   Copyright (C) 2023-2023 Joseph Agrane.
This file is part of matlib.
matlib is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation; either version 3, or (at your option) any later
version.
matlib is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.
You should have received a copy of the GNU General Public License
along with matlib; see the file COPYING. If not see
<http://www.gnu.org/licenses/>. */
/** \file error_definitions.h */
#ifndef INCLUDE_MATLIB_ERROR_DEFINITIONS_H_
#define INCLUDE_MATLIB_ERROR_DEFINITIONS_H_

/** No error occured. */
#define MATLIB_STATUS_OK                     (uint8_t)0
/** One of the function parameters was of type \ref matrix_t
 *  and had not been initialized with \ref matlib_matrix_create.
 */
#define MATLIB_STATUS_UNINITIALIZED_MATRIX   (uint8_t)1
/**
 * At least two function parameters were of type \ref matrix_t
 * and did not have the same \ref matrix_t.dtype
*/
#define MATLIB_STATUS_INCOMPATIBLE_DTYPE     (uint8_t)2
/**
 * At least two function parameters were of type \ref matrix_y
 * and did not have the same \ref matrix_t.width
 * or \ref matrix_t.height
*/
#define MATLIB_STATUS_INCOMPATIBLE_SHAPE     (uint8_t)3
/**
 * An index parameter was out of the range of acceptable values.
*/
#define MATLIB_STATUS_INDEX_OUT_OF_BOUNDS    (uint8_t)4

#endif  // INCLUDE_MATLIB_ERROR_DEFINITIONS_H_
