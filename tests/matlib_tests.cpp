/* matlib unit tests file.
   Copyright (C) 2023-2023 Joseph Agrane.
This file is part of matlib.
matlib is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation; either version 3, or (at your option) any later
version.
matlib is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.
You should have received a copy of the GNU General Public License
along with matlib; see the file COPYING. If not see
<http://www.gnu.org/licenses/>. */
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <vector>

#define EPS 1E-3

extern "C"
{
    
#include <matlib/matlib.h>

}

#define compare_matrices(m1, m2, t) \
    { \
        std::vector<t> m1contents((t*)((m1).data), ((t*)((m1).data)) + (m1).width * (m1).height); \
        std::vector<t> m2contents((t*)((m2).data), ((t*)((m2).data)) + (m2).width * (m2).height); \
        EXPECT_THAT(m1contents, ::testing::Pointwise(::testing::FloatNear(EPS), m2contents)); \
    }

class matrix_t_Tests : public ::testing::Test {
public:

    std::vector<matrix_t> matrices;
    size_t numMatrices = 19;

    void SetUp() override
    {
        matrices = std::vector<matrix_t>(numMatrices);

        matlib_matrix_create(&matrices[0], MATLIB_UINT8, 1, 1);
        ((uint8_t*)matrices[0].data)[0] = 255;

        matlib_matrix_create(&matrices[1], MATLIB_UINT32, 6, 5);
        matlib_matrix_create(&matrices[2], MATLIB_UINT32, 6, 5);
        matlib_matrix_create(&matrices[3], MATLIB_FLOAT32, 2, 3);

        matlib_matrix_create(&matrices[4], MATLIB_INT32, 2, 2);
        int32_t m4contents[4] = {1,5,3,2};
        memcpy(matrices[4].data, m4contents, sizeof(int32_t)*4);

        matlib_matrix_create(&matrices[5], MATLIB_INT32, 2, 2);
        int32_t m5contents[4] = {1,0,0,1};
        memcpy(matrices[5].data, m5contents, sizeof(int32_t)*4);

        matlib_matrix_create(&matrices[6], MATLIB_FLOAT64, 2, 2);
        double m6contents[4] = {1.74, -3.21, 2.9, -10.17};
        memcpy(matrices[6].data, m6contents, sizeof(double)*4);

        matlib_matrix_create(&matrices[7], MATLIB_FLOAT64, 2, 2);
        double m7contents[4] = {1.0, 0.0, 0.0, 1.0};
        memcpy(matrices[7].data, m7contents, sizeof(double)*4);

        matlib_matrix_create(&matrices[8], MATLIB_INT32, 3, 3);
        int32_t m8contents[9] = {3, 7, 4, 4, 9, 7, 8, 6, 2};
        memcpy(matrices[8].data, m8contents, sizeof(int32_t)*9);

        matlib_matrix_create(&matrices[9], MATLIB_INT32, 3, 1);
        int32_t m9contents[3] = {7, 2, 8};
        memcpy(matrices[9].data, m9contents, sizeof(int32_t)*3);
        
        matlib_matrix_create(&matrices[10], MATLIB_INT32, 3, 1);
        int32_t m10contents[3] = {67, 102, 84};
        memcpy(matrices[10].data, m10contents, sizeof(int32_t)*3);

        matlib_matrix_create(&matrices[11], MATLIB_FLOAT32, 2, 2);
        float m11contents[4] = {1.32f, 2.32f, -1.5f, 0.0f};
        memcpy(matrices[11].data, m11contents, sizeof(float)*4);

        matlib_matrix_create(&matrices[12], MATLIB_FLOAT32, 2, 1);
        float m12contents[2] = {6.54f, 6.54f};
        memcpy(matrices[12].data, m12contents, sizeof(float)*2);

        matlib_matrix_create(&matrices[13], MATLIB_FLOAT32, 2, 1);
        float m13contents[2] = {23.8056f, -9.81f};
        memcpy(matrices[13].data, m13contents, sizeof(float)*2);

        matlib_matrix_create(&matrices[14], MATLIB_INT32, 1, 1);
        int32_t m14contents[1] = {-300456};
        memcpy(matrices[14].data, m14contents, sizeof(int32_t)*1);

        matlib_matrix_create(&matrices[15], MATLIB_INT32, 1, 1);
        int32_t m15contents[1] = {-1};
        memcpy(matrices[15].data, m15contents, sizeof(int32_t)*1);

        matlib_matrix_create(&matrices[16], MATLIB_INT32, 1, 1);
        int32_t m16contents[1] = {300456};
        memcpy(matrices[16].data, m16contents, sizeof(int32_t)*1);

        matlib_matrix_create(&matrices[17], MATLIB_INT32, 4, 4);
        int32_t m17contents[16] = {
            -5, 2, 17, 5,
            -9, -4, 5, 6,
            -12, 3, 17, 44,
            0, -12, 3, 5
        };
        memcpy(matrices[17].data, m17contents, sizeof(int32_t)*16);

        matlib_matrix_create(&matrices[18], MATLIB_FLOAT64, 4, 4);
        double m18contents[16] = {
            3.5f, 2.2f, 0.8f, -4.0f,
            0.0f, 0.0f, 1.0f, 3.0f,
            4.4f, 12.0f, -2.3f, -1.3f,
            -4.0f, 3.3f, 3.3f, 0.0f
        };
        memcpy(matrices[18].data, m18contents, sizeof(m18contents));

    }

    void TearDown() override
    {
        for(matrix_t& matrix : matrices)
            matlib_matrix_free(&matrix);
    }

};

TEST_F(matrix_t_Tests, testConstructorAndDestructor)
{
    matrix_t mat;
    // constructor
    matlib_matrix_create(&mat, MATLIB_INT16, 5, 6);
    EXPECT_EQ(mat.height, 5);
    EXPECT_EQ(mat.width, 6);
    EXPECT_EQ(mat.dtype, MATLIB_INT16);
    EXPECT_EQ(mat.elemSize, sizeof(int16_t));
    EXPECT_NE(mat.data, nullptr);
    // destructor
    matlib_matrix_free(&mat);
    EXPECT_EQ(mat.data, nullptr);
}

TEST_F(matrix_t_Tests, testFill)
{
    uint32_t value = 4;
    matlib_matrix_fill(&matrices[1], (void*)(&value));
    for (size_t i = 0; i < matrices[1].height * matrices[1].width; i++)
    {
        EXPECT_EQ(((uint32_t*)matrices[1].data)[i], value) << "i: " << i << std::endl;
    }
}

TEST_F(matrix_t_Tests, testIndexing)
{
    size_t zero = 0;
    matlib_matrix_fill(&matrices[1], &zero);
    // set elements (0,4), (3,5), (5,5) manually
    uint32_t *matdataflat = (uint32_t*)matrices[1].data;
    matdataflat[4] = 10;
    matdataflat[20] = 15;
    matdataflat[29] = 20;
    // get data as multidim array
    uint32_t (*matdata)[5] = (uint32_t (*) [5])matrices[1].data;
    EXPECT_EQ(matdata[0][4], 10);
    EXPECT_EQ(matdata[3][5], 15);
    EXPECT_EQ(matdata[5][4], 20);
}

TEST_F(matrix_t_Tests, copyShouldWork)
{
    uint32_t (*mat1data)[5] = (uint32_t (*) [5])matrices[1].data;
    mat1data[0][4] = 10;
    mat1data[3][5] = 15;
    mat1data[5][4] = 20;
    uint8_t errorCode = matlib_matrix_copy(&matrices[1], &matrices[2]);
    EXPECT_EQ(errorCode, MATLIB_STATUS_OK);
    uint32_t (*mat2data)[5] = (uint32_t (*) [5])matrices[2].data;
    EXPECT_EQ(mat2data[0][4], 10);
    EXPECT_EQ(mat2data[3][5], 15);
    EXPECT_EQ(mat2data[5][4], 20);
}

TEST_F(matrix_t_Tests, copyShouldFail)
{
    matrices[2].dtype = MATLIB_FLOAT32;
    uint8_t errorCode = matlib_matrix_copy(&matrices[1], &matrices[2]);
    EXPECT_EQ(errorCode, MATLIB_STATUS_INCOMPATIBLE_DTYPE) <<
        "operation should have returned incompatible shape error code.\n";
    matrices[2].dtype = MATLIB_UINT32;
    matrices[2].width--;
    errorCode = matlib_matrix_copy(&matrices[1], &matrices[2]);
    EXPECT_EQ(errorCode, MATLIB_STATUS_INCOMPATIBLE_SHAPE) <<
        "operation should have returned incompatible shape error code.\n";
    matrices[2].width++;
    matlib_matrix_free(&matrices[2]);
    errorCode = matlib_matrix_copy(&matrices[1], &matrices[2]);
    EXPECT_EQ(errorCode, MATLIB_STATUS_UNINITIALIZED_MATRIX) <<
        "operation should have returned uninitialized matric error code.\n";
}

TEST_F(matrix_t_Tests, getColumnShouldWork)
{

    using ::testing::ElementsAre;

    uint32_t (*mat1data)[5] = (uint32_t (*) [5])matrices[1].data;
    uint32_t (*mat2data)[5] = (uint32_t (*) [5])matrices[2].data;

    mat1data[0][1] = 3;
    mat1data[1][1] = 4;
    mat1data[2][1] = 5;
    mat1data[3][1] = 6;
    mat1data[4][1] = 7;

    mat2data[0][0] = 10;
    mat2data[1][0] = 15;
    mat2data[2][0] = 20;
    mat2data[3][0] = 25;
    mat2data[4][0] = 30;

    uint32_t *mat1col1 = (uint32_t*)malloc(sizeof(uint32_t)*5);
    uint32_t *mat2col0 = (uint32_t*)malloc(sizeof(uint32_t)*5);

    uint8_t errorCode;

    errorCode = matlib_get_column(&matrices[1], 1, (void*)mat1col1);
    EXPECT_EQ(errorCode, MATLIB_STATUS_OK) << "get_column should have returned ok status\n";
    errorCode = matlib_get_column(&matrices[2], 0, (void*)mat2col0);
    EXPECT_EQ(errorCode, MATLIB_STATUS_OK) << "get_column should have returned ok status\n";

    std::vector<uint32_t> v1(mat1col1, mat1col1+5);
    std::vector<uint32_t> v2(mat2col0, mat2col0+5);

    ASSERT_THAT(v1, ElementsAre(3, 4, 5, 6, 7));
    ASSERT_THAT(v2, ElementsAre(10, 15, 20, 25, 30));

    free(mat1col1);
    free(mat2col0);
}

TEST_F(matrix_t_Tests, getColumnShouldFail)
{
    uint8_t errorCode;
    uint32_t *col = (uint32_t*)(new uint32_t[5]);

    errorCode = matlib_get_column(&matrices[1], 15, (void*)col);
    EXPECT_EQ(errorCode, MATLIB_STATUS_INDEX_OUT_OF_BOUNDS) << "expected out of bounds error code\n";
    errorCode = matlib_get_column(&matrices[1], -1, (void*)col);
    EXPECT_EQ(errorCode, MATLIB_STATUS_INDEX_OUT_OF_BOUNDS) << "expected out of bounds error code\n";

    matrices[1].data = nullptr;
    errorCode = matlib_get_column(&matrices[1], 1, (void*)col);
    EXPECT_EQ(errorCode, MATLIB_STATUS_UNINITIALIZED_MATRIX) << "expected unitialized matric error code\n";

    delete[] col;
}

TEST_F(matrix_t_Tests, transposeShouldWork)
{
    using ::testing::ElementsAreArray;

    float (*m3data) [3] = (float (*) [3])matrices[3].data;
    std::vector<std::vector<float>> expected = {
        {-3.5f, 2.5f},
        {5.0f, 10.0f},
        {0.0f, -5.0f}
    };
    for(size_t i = 0; i < 3; i++)
        for(size_t j = 0; j < 2; j++)
            m3data[j][i] = expected[i][j];

    matrix_t matrix3t;
    matlib_matrix_create(&matrix3t, matrices[3].dtype, matrices[3].height, matrices[3].width);
    matlib_matrix_copy(&matrices[3], &matrix3t);
    uint8_t errorCode = matlib_matrix_transpose(&matrix3t);
    EXPECT_EQ(errorCode, MATLIB_STATUS_OK) << "transpose should have returned ok status\n";

    std::vector<std::vector<float>> result = {{},{},{}};
    result[0] = std::vector<float>(((float*)matrix3t.data), ((float*)matrix3t.data)+2);
    result[1] = std::vector<float>(((float*)matrix3t.data)+2, ((float*)matrix3t.data)+4);
    result[2] = std::vector<float>(((float*)matrix3t.data)+4, ((float*)matrix3t.data)+6);

    EXPECT_THAT(result, ElementsAreArray(expected));
}

TEST_F(matrix_t_Tests, transposeShouldFail)
{
    matrices[3].data = nullptr;
    uint8_t errorCode = matlib_matrix_transpose(&matrices[3]);
    EXPECT_EQ(errorCode, MATLIB_STATUS_UNINITIALIZED_MATRIX) << "transpose should have returned unitialized status code.\n";
}

TEST_F(matrix_t_Tests, mulShouldWork)
{
    matrix_t result;
    int32_t zero = 0;
    uint8_t error;
    double zerodbl = 0.0;
    float zerof = 0.0f;

    matlib_matrix_create(&result, MATLIB_INT32, 2, 2);
    matlib_matrix_fill(&result, &zero);
    error = matlib_matrix_mul(&result, &matrices[4], &matrices[5]);
    compare_matrices(result, matrices[4], int32_t);
    EXPECT_EQ(error, MATLIB_STATUS_OK) << "mul should have returned ok status.\n";

    matlib_matrix_fill(&result, &zero);
    error = matlib_matrix_mul(&result, &matrices[5], &matrices[4]);
    compare_matrices(result, matrices[4], int32_t);
    EXPECT_EQ(error, MATLIB_STATUS_OK) << "mul should have returned ok status.\n";

    matlib_matrix_free(&result);
    matlib_matrix_create(&result, MATLIB_FLOAT64, 2, 2);
    matlib_matrix_fill(&result, &zerodbl);
    error = matlib_matrix_mul(&result, &matrices[6], &matrices[7]);
    compare_matrices(result, matrices[6], double);
    EXPECT_EQ(error, MATLIB_STATUS_OK) << "mul should have returned ok status.\n";

    matlib_matrix_fill(&result, &zerodbl);
    error = matlib_matrix_mul(&result, &matrices[7], &matrices[6]);
    compare_matrices(result, matrices[6], double);
    EXPECT_EQ(error, MATLIB_STATUS_OK) << "mul should have returned ok status.\n";

    matlib_matrix_free(&result);
    matlib_matrix_create(&result, MATLIB_INT32, 3, 1);
    matlib_matrix_fill(&result, &zero);
    error = matlib_matrix_mul(&result, &matrices[8], &matrices[9]);
    compare_matrices(result, matrices[10], int32_t);
    EXPECT_EQ(error, MATLIB_STATUS_OK) << "mul should have returned ok status.\n";

    matlib_matrix_free(&result);
    matlib_matrix_create(&result, MATLIB_FLOAT32, 2, 1);
    matlib_matrix_fill(&result, &zerof);
    error = matlib_matrix_mul(&result, &matrices[11], &matrices[12]);
    compare_matrices(result, matrices[13], float);
    EXPECT_EQ(error, MATLIB_STATUS_OK) << "mul should have returned ok status.\n";

    matlib_matrix_free(&result);
    matlib_matrix_create(&result, MATLIB_INT32, 1, 1);
    matlib_matrix_fill(&result, &zero);
    error = matlib_matrix_mul(&result, &matrices[14], &matrices[15]);
    compare_matrices(result, matrices[16], int32_t);
    EXPECT_EQ(error, MATLIB_STATUS_OK) << "mul should have returned ok status.\n";

    matlib_matrix_free(&result);
}

TEST_F(matrix_t_Tests, mulShouldFail)
{
    uint8_t err;
    // uninitialized
    void* temp = matrices[13].data;
    matrices[13].data = nullptr;
    err = matlib_matrix_mul(&matrices[13], &matrices[11], &matrices[12]);
    EXPECT_EQ(err, MATLIB_STATUS_UNINITIALIZED_MATRIX) << "mul should have returned uninit error code.\n";
    matrices[13].data = temp; // restore data so free still works in teardown

    // incompatible types
    matrix_t result;
    matlib_matrix_create(&result, MATLIB_FLOAT32, 2, 1);
    err = matlib_matrix_mul(&result, &matrices[7], &matrices[12]);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_DTYPE) << "mul should have returned dtype error code.\n";

    // incomatible shape
    err = matlib_matrix_mul(&result, &matrices[12], &matrices[13]);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_SHAPE) << "mul should have returned shape error code.\n";

    matlib_matrix_free(&result);
}

TEST_F(matrix_t_Tests, addShouldWork)
{
    uint8_t err;

    matrix_t result, expected;

    matlib_matrix_create(&result, MATLIB_INT32, 2, 2);
    matlib_matrix_create(&expected, MATLIB_INT32, 2, 2);
    {
        int32_t x[4] = {2, 5, 3, 3};
        memcpy(expected.data, x, sizeof(int32_t)*4);
    }
    err = matlib_matrix_add(&result, &matrices[4], &matrices[5]);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, expected, int32_t);
    matlib_matrix_free(&result);
    matlib_matrix_free(&expected);

    matlib_matrix_create(&result, MATLIB_FLOAT64, 2, 2);
    matlib_matrix_create(&expected, MATLIB_FLOAT64, 2, 2);
    {
        double x[4] = {2.74, -3.21, 2.9, -9.17};
        memcpy(expected.data, x, sizeof(double)*4);
    }
    err = matlib_matrix_add(&result, &matrices[6], &matrices[7]);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, expected, double);
    matlib_matrix_free(&result);
    matlib_matrix_free(&expected);

    matlib_matrix_create(&result, MATLIB_INT32, 1, 1);
    matlib_matrix_create(&expected, MATLIB_INT32, 1, 1);
    ((int32_t*)expected.data)[0] = -300457;
    err = matlib_matrix_add(&result, &matrices[14], &matrices[15]);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, expected, double);
    matlib_matrix_free(&result);
    matlib_matrix_free(&expected);

}

TEST_F(matrix_t_Tests, addShouldFail)
{
    uint8_t err;
    matrix_t mat;
    mat.data = NULL;
    // uninitialized matrix
    err = matlib_matrix_add(&mat, &matrices[4], &matrices[5]);
    EXPECT_EQ(err, MATLIB_STATUS_UNINITIALIZED_MATRIX);
    // bad shape
    matlib_matrix_create(&mat, MATLIB_INT32, 4, 4);
    err = matlib_matrix_add(&mat, &matrices[4], &matrices[5]);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_SHAPE);
    matlib_matrix_free(&mat);
    // bad dtype
    matlib_matrix_create(&mat, MATLIB_INT32, 2, 2);
    matrices[4].dtype = MATLIB_FLOAT32;
    err = matlib_matrix_add(&mat, &matrices[4], &matrices[5]);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_DTYPE);
    matlib_matrix_free(&mat);
}

TEST_F(matrix_t_Tests, subShouldWork)
{
    uint8_t err;

    matrix_t result, expected;

    matlib_matrix_create(&result, MATLIB_INT32, 2, 2);
    matlib_matrix_create(&expected, MATLIB_INT32, 2, 2);
    {
        int32_t x[4] = {0, 5, 3, 1};
        memcpy(expected.data, x, sizeof(int32_t)*4);
    }
    err = matlib_matrix_sub(&result, &matrices[4], &matrices[5]);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, expected, int32_t);
    matlib_matrix_free(&result);
    matlib_matrix_free(&expected);

    matlib_matrix_create(&result, MATLIB_FLOAT64, 2, 2);
    matlib_matrix_create(&expected, MATLIB_FLOAT64, 2, 2);
    {
        double x[4] = {0.74, -3.21, 2.9, -11.17};
        memcpy(expected.data, x, sizeof(double)*4);
    }
    err = matlib_matrix_sub(&result, &matrices[6], &matrices[7]);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, expected, double);
    matlib_matrix_free(&result);
    matlib_matrix_free(&expected);

    matlib_matrix_create(&result, MATLIB_INT32, 1, 1);
    matlib_matrix_create(&expected, MATLIB_INT32, 1, 1);
    ((int32_t*)expected.data)[0] = 0;
    err = matlib_matrix_sub(&result, &matrices[15], &matrices[15]);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, expected, int32_t);
    matlib_matrix_free(&result);
    matlib_matrix_free(&expected);

}

TEST_F(matrix_t_Tests, subShouldFail)
{
    uint8_t err;
    matrix_t mat;
    mat.data = NULL;
    // uninitialized matrix
    err = matlib_matrix_sub(&mat, &matrices[4], &matrices[5]);
    EXPECT_EQ(err, MATLIB_STATUS_UNINITIALIZED_MATRIX);
    // bad shape
    matlib_matrix_create(&mat, MATLIB_INT32, 4, 4);
    err = matlib_matrix_sub(&mat, &matrices[4], &matrices[5]);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_SHAPE);
    matlib_matrix_free(&mat);
    // bad dtype
    matlib_matrix_create(&mat, MATLIB_INT32, 2, 2);
    matrices[4].dtype = MATLIB_FLOAT32;
    err = matlib_matrix_sub(&mat, &matrices[4], &matrices[5]);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_DTYPE);
    matlib_matrix_free(&mat);
}

TEST_F(matrix_t_Tests, scalarMulShouldWork)
{
    matrix_t expected;
    uint8_t err;

    float mtwof = -2.0f;

    matlib_matrix_create(&expected, MATLIB_FLOAT32, 2, 2);
    {
        float x[4] = {
            -2.64f, -4.64f,
            3.0f, 0.0f
        };
        memcpy(expected.data, x, sizeof(x));
    }
    err = matlib_scalar_mul(&matrices[11], (void*)&mtwof);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(matrices[11], expected, float);
    matlib_matrix_free(&expected);
}

TEST_F(matrix_t_Tests, scalarDivShouldWork)
{
    matrix_t expected;
    uint8_t err;

    float mtwof = -2.0f;

    matlib_matrix_create(&expected, MATLIB_FLOAT32, 2, 2);
    {
        float x[4] = {
            -0.66f, -1.16f,
            0.75f, 0.0f
        };
        memcpy(expected.data, x, sizeof(x));
    }
    err = matlib_scalar_div(&matrices[11], (void*)&mtwof);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(matrices[11], expected, float);
    matlib_matrix_free(&expected);
}

TEST_F(matrix_t_Tests, coverColRowShouldWork)
{

    matrix_t result, expected;
    uint8_t err;

    matlib_matrix_create(&result, MATLIB_FLOAT32, 1, 1);
    matlib_matrix_create(&expected, MATLIB_FLOAT32, 1, 1);
    ((float*)expected.data)[0] = 1.32f;
    err = matlib_cover_column_row(&result, &matrices[11], 1, 1);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, expected, float);
    matlib_matrix_free(&result);
    matlib_matrix_free(&expected);

    matlib_matrix_create(&result, MATLIB_INT32, 3, 3);
    matlib_matrix_create(&expected, MATLIB_INT32, 3, 3);
    {
        int32_t x[9] = {
            -5, 17, 5,
            -12, 17, 44,
            0, 3, 5
        };
        memcpy(expected.data, x, sizeof(int32_t)*9);
    }
    err = matlib_cover_column_row(&result, &matrices[17], 1, 1);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, expected, int32_t);
    matlib_matrix_free(&result);
    matlib_matrix_free(&expected);

    matlib_matrix_create(&result, MATLIB_FLOAT64, 3, 3);
    matlib_matrix_create(&expected, MATLIB_FLOAT64, 3, 3);
    {
        double x[9] = {
            0.0, 1.0, 3.0,
            4.4, -2.3, -1.3,
            -4.0, 3.3, 0.0
        };
        memcpy(expected.data, x, sizeof(double)*9);
    }
    err = matlib_cover_column_row(&result, &matrices[18], 0, 1);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, expected, double);
    matlib_matrix_free(&result);
    matlib_matrix_free(&expected);
}

TEST_F(matrix_t_Tests, coverColRowShouldFail)
{
    matrix_t result;
    uint8_t err;

    // result has wrong dimensions
    matlib_matrix_create(&result, MATLIB_FLOAT32, 2, 2);
    err = matlib_cover_column_row(&result, &matrices[11], 0, 0);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_SHAPE);
    matlib_matrix_free(&result);

    // result has wrong dtype
    matlib_matrix_create(&result, MATLIB_INT32, 1, 1);
    err = matlib_cover_column_row(&result, &matrices[11], 0, 0);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_DTYPE);
    matlib_matrix_free(&result);

    // result is uninitialized
    err = matlib_cover_column_row(&result, &matrices[11], 0, 0);
    EXPECT_EQ(err, MATLIB_STATUS_UNINITIALIZED_MATRIX);

    // input is non-square
    matlib_matrix_create(&result, MATLIB_FLOAT32, 5, 4);
    err = matlib_cover_column_row(&result, &matrices[1], 1, 1);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_SHAPE);
    matlib_matrix_free(&result);

    // input is 1x1
    matlib_matrix_create(&result, MATLIB_UINT8, 1, 1);
    result.width = 0;
    result.height = 0;
    err = matlib_cover_column_row(&result, &matrices[0], 0, 0);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_SHAPE);
    matlib_matrix_free(&result);

    // column out of range
    matlib_matrix_create(&result, MATLIB_FLOAT32, 1, 1);
    err = matlib_cover_column_row(&result, &matrices[11], 1, 2);
    EXPECT_EQ(err, MATLIB_STATUS_INDEX_OUT_OF_BOUNDS);
    matlib_matrix_free(&result);

    // row out of range
    matlib_matrix_create(&result, MATLIB_FLOAT32, 1, 1);
    err = matlib_cover_column_row(&result, &matrices[11], 2, 0);
    EXPECT_EQ(err, MATLIB_STATUS_INDEX_OUT_OF_BOUNDS);
    matlib_matrix_free(&result);
}

TEST_F(matrix_t_Tests, determinantShouldWork)
{

    uint8_t err;
    float f32det = 0.0f;
    double f64det = 0.0;
    int32_t i32det = 0;

    err = matlib_matrix_determinant(&matrices[17], &i32det);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    EXPECT_EQ(i32det, -60535);

    err = matlib_matrix_determinant(&matrices[11], &f32det);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    EXPECT_FLOAT_EQ(f32det, 3.48f);

    err = matlib_matrix_determinant(&matrices[8], &i32det);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    EXPECT_EQ(i32det, 72);

    err = matlib_matrix_determinant(&matrices[7], &f64det);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    EXPECT_DOUBLE_EQ(f64det, 1.0);

    err = matlib_matrix_determinant(&matrices[6], &f64det);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    EXPECT_DOUBLE_EQ(f64det, -8.3868);

    err = matlib_matrix_determinant(&matrices[18], &f64det);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    EXPECT_NEAR(f64det, 834.056, EPS);

}

TEST_F(matrix_t_Tests, determinantShouldFail)
{

    uint8_t err;
    matrix_t mat;

    uint8_t buffer[16] = {0};

    // non-square
    matlib_matrix_create(&mat, MATLIB_INT32, 3, 4);
    err = matlib_matrix_determinant(&mat, (void*)buffer);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_SHAPE);

    // uninitialized
    matlib_matrix_free(&mat);
    err = matlib_matrix_determinant(&mat, (void*)buffer);
    EXPECT_EQ(err, MATLIB_STATUS_UNINITIALIZED_MATRIX);

}

TEST_F(matrix_t_Tests, cofactorsShouldWork)
{

    matrix_t expected, result;
    uint8_t err;

    matlib_matrix_create(&result, MATLIB_INT32, 4, 4);
    matlib_matrix_create(&expected, MATLIB_INT32, 4, 4);
    {
        int32_t x[16] = {
            -1249, -507, -4263, 1341,
            8260, 1075, 1875, 1455,
            -630, -595, 370, -1650,
            -3119, 4453, -1243, -674
        };
        memcpy(expected.data, x, sizeof(x));
    }
    err = matlib_matrix_of_cofactors(&result, &matrices[17]);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, expected, int32_t);
    matlib_matrix_free(&result);
    matlib_matrix_free(&expected);

    matlib_matrix_create(&result, MATLIB_FLOAT32, 2, 2);
    matlib_matrix_create(&expected, MATLIB_FLOAT32, 2, 2);
    {
        float x[4] = {
            0.0f, 1.5f, -2.32f, 1.32f
        };
        memcpy(expected.data, x, sizeof(x));
    }
    err = matlib_matrix_of_cofactors(&result, &matrices[11]);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, expected, float);
    matlib_matrix_free(&result);
    matlib_matrix_free(&expected);

    matlib_matrix_create(&result, MATLIB_INT32, 3, 3);
    matlib_matrix_create(&expected, MATLIB_INT32, 3, 3);
    {
        int32_t x[9] = {
            -24, 48, -48,
            10, -26, 38,
            13, -5, -1
        };
        memcpy(expected.data, x, sizeof(x));
    }
    err = matlib_matrix_of_cofactors(&result, &matrices[8]);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, expected, int32_t);
    matlib_matrix_free(&result);
    matlib_matrix_free(&expected);
}

TEST_F(matrix_t_Tests, cofactorShouldFail)
{

    uint8_t err;

    matrix_t result;


    // non-square
    matlib_matrix_create(&result, MATLIB_FLOAT32, 2, 3);
    err = matlib_matrix_of_cofactors(&result, &matrices[3]);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_SHAPE);
    matlib_matrix_free(&result);

    // uninit
    err = matlib_matrix_of_cofactors(&result, &matrices[3]);
    EXPECT_EQ(err, MATLIB_STATUS_UNINITIALIZED_MATRIX);

    // wrong result columns
    matlib_matrix_create(&result, MATLIB_INT32, 2, 3);
    err = matlib_matrix_of_cofactors(&result, &matrices[5]);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_SHAPE);

    // wrong result rows
    matlib_matrix_create(&result, MATLIB_INT32, 3, 2);
    err = matlib_matrix_of_cofactors(&result, &matrices[5]);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_SHAPE);

    // bad dtype
    matlib_matrix_create(&result, MATLIB_INT16, 2, 2);
    err = matlib_matrix_of_cofactors(&result, &matrices[5]);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_DTYPE);

}

TEST_F(matrix_t_Tests, inverseShouldWork)
{

    matrix_t result, expected;
    uint8_t err;

    matlib_matrix_create(&result, MATLIB_FLOAT64, 2, 2);
    matlib_matrix_create(&expected, MATLIB_FLOAT64, 2, 2);
    {
        double x[4] = {
            1.2126198311632565459, -0.38274431249105737586,
            0.34578146611341632087, -0.20746887966804979253
        };
        memcpy(expected.data, x, sizeof(x));
    }
    err = matlib_matrix_inverse(&result, &matrices[6]);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, expected, double);
    matlib_matrix_free(&result);
    matlib_matrix_free(&expected);

    matlib_matrix_create(&result, MATLIB_FLOAT32, 2, 2);
    matlib_matrix_create(&expected, MATLIB_FLOAT32, 2, 2);
    {
        float x[4] = {
            0.0, -0.66666666666666666666,
            0.43103448275862068966, 0.37931034482758620691
        };
        memcpy(expected.data, x, sizeof(x));
    }
    err = matlib_matrix_inverse(&result, &matrices[11]);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, expected, float);
    matlib_matrix_free(&result);
    matlib_matrix_free(&expected);

    matlib_matrix_create(&result, MATLIB_FLOAT64, 2, 2);
    err = matlib_matrix_inverse(&result, &matrices[7]);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, matrices[7], double);
    matlib_matrix_free(&result);

    matlib_matrix_create(&result, MATLIB_FLOAT64, 4, 4);
    matlib_matrix_create(&expected, MATLIB_FLOAT64, 4, 4);
    {
        double x[16] = {
            0.16459326472083409268f, 0.21911478365961038584f, -0.000791313772696317756f, -0.10685133851923611843f,
            -0.02536999913674861161f, -0.00252381135079658917f, 0.072237355765080522168f, 0.057262342096933539232f,
            0.22487698667715357242f, 0.26811748851396069328f, -0.073196523974409392172f, 0.11625118697065904448f,
            -0.074958995559051190808f, 0.24396083716201310224f, 0.024398841324803130724f, -0.038750395656886348162f
        };
        memcpy(expected.data, x, sizeof(x));
    }
    err = matlib_matrix_inverse(&result, &matrices[18]);
    EXPECT_EQ(err, MATLIB_STATUS_OK);
    compare_matrices(result, expected, double);
    matlib_matrix_free(&result);
    matlib_matrix_free(&expected);

}

TEST_F(matrix_t_Tests, inverseShouldFail)
{

    matrix_t result, mat;
    uint8_t err;

    // non-square input
    matlib_matrix_create(&result, MATLIB_UINT32, 6, 5);
    err = matlib_matrix_inverse(&result, &matrices[1]);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_SHAPE);
    matlib_matrix_free(&result);

    // bad result shape
    matlib_matrix_create(&result, MATLIB_INT32, 3, 3);
    err = matlib_matrix_inverse(&result, &matrices[4]);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_SHAPE);
    matlib_matrix_free(&result);

    // wrong dtype
    matlib_matrix_create(&result, MATLIB_UINT32, 2, 2);
    err = matlib_matrix_inverse(&result, &matrices[4]);
    EXPECT_EQ(err, MATLIB_STATUS_INCOMPATIBLE_DTYPE);
    matlib_matrix_free(&result);

    // uninitialized input
    mat.data = NULL;
    matlib_matrix_create(&result, MATLIB_INT32, 2, 2);
    err = matlib_matrix_inverse(&result, &mat);
    EXPECT_EQ(err, MATLIB_STATUS_UNINITIALIZED_MATRIX);
    matlib_matrix_free(&result);

    // uninitialized result
    err = matlib_matrix_inverse(&result, &matrices[6]);
    EXPECT_EQ(err, MATLIB_STATUS_UNINITIALIZED_MATRIX);
}
