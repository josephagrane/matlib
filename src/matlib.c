/* matlib implementation file.
   Copyright (C) 2023-2023 Joseph Agrane.
This file is part of matlib.
matlib is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation; either version 3, or (at your option) any later
version.
matlib is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.
You should have received a copy of the GNU General Public License
along with matlib; see the file COPYING. If not see
<http://www.gnu.org/licenses/>. */
#include <matlib/matlib.h>

#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>

int matlib_matrix_create(
    matrix_t *pMatrix,
    uint8_t dtype,
    size_t height,
    size_t width) {
    pMatrix->dtype = dtype;
    pMatrix->width = width;
    pMatrix->height = height;
    // get the size in bytes corresponding to dtype
    MATLIB_DTYPE_SIZE((pMatrix->elemSize), dtype);
    pMatrix->data = malloc(width*height*pMatrix->elemSize);
    return MATLIB_STATUS_OK;
}

int matlib_matrix_free(matrix_t *pMatrix) {
    free(pMatrix->data);
    pMatrix->data = NULL;
    return MATLIB_STATUS_OK;
}

int matlib_matrix_fill(matrix_t *pMatrix, void *value) {
    if (pMatrix->data == NULL)
        return MATLIB_STATUS_UNINITIALIZED_MATRIX;
    size_t elements = pMatrix->width * pMatrix->height;
    for (
        size_t offset = 0;
        offset < elements*pMatrix->elemSize;
        offset += pMatrix->elemSize) {
        memcpy((uint8_t*)(pMatrix->data)+offset, value, pMatrix->elemSize);
    }
    return MATLIB_STATUS_OK;
}

int matlib_matrix_copy(const matrix_t * const src, matrix_t* dest) {
    if (src->data == NULL || dest->data == NULL)
        return MATLIB_STATUS_UNINITIALIZED_MATRIX;

    if (src->dtype != dest->dtype)
        return MATLIB_STATUS_INCOMPATIBLE_DTYPE;

    if (src->width != dest->width || src->height != dest->height)
        return MATLIB_STATUS_INCOMPATIBLE_SHAPE;

    memcpy(dest->data, src->data, src->elemSize * src->width * src->height);

    return MATLIB_STATUS_OK;
}

int matlib_matrix_transpose(matrix_t * pMat) {
    matrix_t mat;  // temporary copy to store result
    matlib_matrix_create(&mat, pMat->dtype, pMat->width, pMat->height);
    if (pMat->data == NULL)
        return MATLIB_STATUS_UNINITIALIZED_MATRIX;

    // get size in bytes of a column
    size_t column_size = pMat->height * pMat->elemSize;
    for (size_t column = 0; column < pMat->width; column++) {
        size_t offset = column_size*column;
        matlib_get_column(pMat, column, ((uint8_t*)mat.data) + offset);
    }
    pMat->height = mat.height;
    pMat->width = mat.width;
    matlib_matrix_copy(&mat, pMat);
    matlib_matrix_free(&mat);
    return MATLIB_STATUS_OK;
}

int matlib_matrix_determinant(
    const matrix_t * const pMatrix,
    void * pDeterminant) {
    if (pMatrix->data == NULL)
        return MATLIB_STATUS_UNINITIALIZED_MATRIX;

    if (pMatrix->width != pMatrix->height)
        return MATLIB_STATUS_INCOMPATIBLE_SHAPE;

    // special case where the determinant is the
    // first element in the matrix's data array.
    if (pMatrix->height == 1) {
        memcpy(pDeterminant, pMatrix->data, pMatrix->elemSize);
        return MATLIB_STATUS_OK;
    }

    uint8_t dtype = pMatrix->dtype;
    size_t elemSize = pMatrix->elemSize;
    size_t n = pMatrix->width;  // == pMatrix->height

    /**
     * 
     * Below is a recursive algorithm which has been implemented in a
     * recursive manner using a stack-like structure.
     * 
     * The recursive algorithm can be described by:
     * 
     * determinant(matrix, m=1)
     * 1.   if matrix is 2x2
     * 2.     return m * (a*d - b*c)
     * 3.   else
     * 4.     submatrices = submatrices of matrix
     * 5.     sum = 0
     * 6.     for submatrix, element in submatrices:
     * 7.       sum += determinant(submatrix, m*element)
     * 8.     return sum
     * 
     * Here we initialize a stack-like structure. Each element on the stack
     * is as follows:
     * sizeof(matrix_t*) bytes : A pointer to a heap-allocated matrix_t
     * pMatrix->elemSize bytes : A pointer to a heap-allocated numerical value
     *                           this number is used as a multiplier.
     * 
     * 1. while the stack is non-empty:
     * 2.   pop an element from the stack
     * 3.   if the element happens to be 2x2:
     * 4.     calculate the determinant and multiply it by
     *        multiplier part of the element
     * 5.     add this to the determinant sum
     * 6.   else:
     * 7.     generate a sub-matrix for each value in the,
     *        first column where the index of that element
     *        is excluded from the sub-matrix.
     * 8.     push each of
     *        these sub-matrices to the stack with their
     *        corresponding element as the multiplier.
     * 
    */

    size_t stackElementSize = sizeof(matrix_t*) + elemSize;

    // The maximum stack size can be proven to be equal to this.
    // The proof is trivial; simply draw a recursion tree for the stack
    // decompositions of a 4x4 input to see the pattern and equation form.
    size_t stackSize = ((n / 2.0) * (n - 1)) * stackElementSize;

    // pointer to the start of the stack
    void * pBase = malloc(stackSize);
    // pointer to the next free slot on the stack
    void * pStack = pBase;

    // ensure the value of the determinant is zero,
    // do this by dynamically casting it depending
    // on the value of dtype.
    uint8_t zero = 0;
    MATLIB_TYPECAST(pDeterminant, zero, dtype);

    // push first element onto stack
    {
        // we need to make a copy of the input matrix
        // as the loop logic will eventuall attempt
        // to call free() on it.
        matrix_t *copy = malloc(sizeof(matrix_t));
        matlib_matrix_create(copy, dtype, pMatrix->height, pMatrix->width);
        matlib_matrix_copy(pMatrix, copy);
        // push the pointer onto the stack
        memcpy(pStack, &copy, sizeof(matrix_t*));
        pStack = ((matrix_t**)pStack) + 1;
        // push an initial multiplier, 1, onto the stack
        uint8_t one = 1;
        void * oneCasted = malloc(elemSize);
        // again, need to dynamically cast it to match the
        // matrix's datatype
        MATLIB_TYPECAST(oneCasted, one, dtype);
        // this is where we push it on
        memcpy(pStack, oneCasted, elemSize);
        pStack = ((uint8_t*)pStack) + elemSize;
        // ------
        free(oneCasted);
    }

    // this is where we store the numeric
    // 'multiplier' value
    void * multiplier = malloc(elemSize);

    // while stack is non-empty
    while (pBase != pStack) {
        // pop multiplier from stack
        pStack = ((uint8_t*)pStack) - elemSize;
        memcpy(multiplier, pStack, elemSize);
        // pop matrix* from stack
        pStack = ((matrix_t**)pStack) - 1;
        matrix_t* matrix = *(matrix_t**)pStack;

        // if dim 2x2
        if (matrix->height == 2) {
            void
                *x = malloc(elemSize),
                *y = malloc(elemSize),
                *z = malloc(elemSize);
            void  *a, *b, *c, *d;
            a = matrix->data;
            b = (uint8_t*)(a) + elemSize;
            c = (uint8_t*)(b) + elemSize;
            d = (uint8_t*)(c) + elemSize;
            MATLIB_UNKOWN_OP(x, a, d, *, dtype);
            MATLIB_UNKOWN_OP(y, b, c, *, dtype);
            // calculate determinant d = a*d - b*c
            MATLIB_UNKOWN_OP(z, x, y, -, dtype);
            // scale by multiplier, d = m * (a*d - b*c)
            MATLIB_UNKOWN_OP(
                z,
                z,
                multiplier,
                *, dtype);
            MATLIB_UNKOWN_OP(
                pDeterminant,
                pDeterminant,
                z, +, dtype);  // add to total
            free(x);
            free(y);
            free(z);
        } else {
            // we need to use an alternating sign
            // for the multiplier of the submatrices
            int8_t sign = 1;
            // this contains the raw data of the sign
            // casted to the type stored in the matrix
            void * signCasted = malloc(elemSize);

            // cover each element in the first row
            for (size_t column = 0;
                column < matrix->width;
                column++, sign *= -1) {
                matrix_t * submatrix = malloc(sizeof(matrix_t));
                void * newMultiplier =
                    ((uint8_t*)matrix->data) + column * elemSize;
                matlib_matrix_create(
                    submatrix, dtype,
                    matrix->height-1,
                    matrix->width-1);
                matlib_cover_column_row(submatrix, matrix, 0, column);

                // push matrix to stack
                memcpy(pStack, &submatrix, sizeof(matrix_t*));
                pStack = ((matrix_t**)pStack) + 1;
                // push new multiplier to stack
                memcpy(pStack, newMultiplier, elemSize);
                // cast sign to correct datatype
                MATLIB_TYPECAST(signCasted, sign, dtype);
                // while multiplier in stack,
                // multiply it by the previous multiplier and sign
                MATLIB_UNKOWN_OP(pStack, pStack, multiplier, *, dtype);
                MATLIB_UNKOWN_OP(pStack, pStack, signCasted, *, dtype);
                pStack = ((uint8_t*)pStack) + elemSize;
            }
            free(signCasted);
        }

        matlib_matrix_free(matrix);
        free(matrix);
    }

    free(multiplier);
    free(pBase);

    return MATLIB_STATUS_OK;
}

int matlib_matrix_inverse(matrix_t * pResult, const matrix_t * const pMatrix) {
    if (pResult->data == NULL || pMatrix->data == NULL)
        return MATLIB_STATUS_UNINITIALIZED_MATRIX;

    if (pMatrix->width != pMatrix->height)
        return MATLIB_STATUS_INCOMPATIBLE_SHAPE;

    if (pResult->width != pMatrix->width || pResult->height != pMatrix->height)
        return MATLIB_STATUS_INCOMPATIBLE_SHAPE;

    if (pResult->dtype != pMatrix->dtype)
        return MATLIB_STATUS_INCOMPATIBLE_DTYPE;

    // calculate adj(m)
    matlib_matrix_of_cofactors(pResult, pMatrix);
    matlib_matrix_transpose(pResult);

    void * determinant = malloc(pMatrix->elemSize);

    matlib_matrix_determinant(pMatrix, determinant);

    // inv = (1/|m|) * adj(m)
    matlib_scalar_div(pResult, determinant);

    free(determinant);
    return MATLIB_STATUS_OK;
}

int matlib_matrix_mul(
    matrix_t* pInput,
    const matrix_t * const pLhs,
    const matrix_t * const pRhs) {

    if (pInput->data == NULL || pLhs->data == NULL || pRhs->data == NULL)
        return MATLIB_STATUS_UNINITIALIZED_MATRIX;

    if (pLhs->width != pRhs->height
        ||
        pInput->height != pLhs->height
        ||
        pInput->width != pRhs->width)
        return MATLIB_STATUS_INCOMPATIBLE_SHAPE;

    if (pInput->dtype != pLhs->dtype
        ||
        pLhs->dtype != pRhs->dtype
        ||
        pRhs->dtype != pInput->dtype)
        return MATLIB_STATUS_INCOMPATIBLE_DTYPE;

    matrix_t result;
    matlib_matrix_create(
        &result,
        pInput->dtype,
        pInput->height,
        pInput->width);
    int8_t zero = 0;
    void * zerocasted = malloc(pInput->dtype);
    MATLIB_TYPECAST(zerocasted, zero, pInput->dtype);
    matlib_matrix_fill(
        &result,
        zerocasted);
    free(zerocasted);

    size_t elemSize = pInput->elemSize;
    uint8_t dtype = pInput->dtype;

    void *rhscolumn = malloc(elemSize * pLhs->height);
    void *elem = malloc(elemSize);

    for (size_t lhsrowindex = 0; lhsrowindex < pLhs->height; lhsrowindex++) {
        void *newrow =
            ((uint8_t*)result.data) + lhsrowindex * pRhs->width * elemSize;
        void *lhsrow =
            ((uint8_t*)pLhs->data) + lhsrowindex * pLhs->width * elemSize;
        for (size_t rhscol = 0; rhscol < pRhs->width; rhscol++) {
            matlib_get_column(pRhs, rhscol, rhscolumn);
            for (size_t elemOffset = 0;
                elemOffset < pLhs->width * elemSize;
                elemOffset += elemSize) {
                MATLIB_UNKOWN_OP(
                    ((uint8_t*)elem),
                    ((uint8_t*)lhsrow) + elemOffset,
                    ((uint8_t*)rhscolumn) + elemOffset,
                    *, dtype);
                MATLIB_UNKOWN_OP(
                    ((uint8_t*)newrow) + rhscol * elemSize,
                    ((uint8_t*)elem),
                    ((uint8_t*)newrow) + rhscol * elemSize,
                    +, dtype);
            }
        }
    }

    matlib_matrix_copy(&result, pInput);
    matlib_matrix_free(&result);
    free(rhscolumn);
    free(elem);

    return MATLIB_STATUS_OK;
}

int matlib_matrix_add(
    matrix_t* pResult,
    const matrix_t * const pLhs,
    const matrix_t * const pRhs) {
    if (pResult->data == NULL || pLhs->data == NULL || pRhs->data == NULL)
        return MATLIB_STATUS_UNINITIALIZED_MATRIX;

    if (pResult->width != pLhs->width
        ||
        pResult->height != pLhs->height
        ||
        pLhs->width != pRhs->width
        ||
        pLhs->height != pRhs->width)
        return MATLIB_STATUS_INCOMPATIBLE_SHAPE;

    if (pResult->dtype != pLhs->dtype || pLhs->dtype != pRhs->dtype)
        return MATLIB_STATUS_INCOMPATIBLE_DTYPE;

    size_t
        elemSize = pResult->elemSize,
        totalSize = pResult->width * pResult->height * pResult->elemSize;

    for (size_t offset = 0; offset < totalSize; offset += elemSize) {
        MATLIB_UNKOWN_OP(
            ((uint8_t*)pResult->data) + offset,
            ((uint8_t*)pLhs->data) + offset,
            ((uint8_t*)pRhs->data) + offset,
            +, pResult->dtype);
    }

    return MATLIB_STATUS_OK;
}

int matlib_matrix_sub(
    matrix_t* pResult,
    const matrix_t * const pLhs,
    const matrix_t * const pRhs) {
    if (pResult->data == NULL
        ||
        pLhs->data == NULL
        ||
        pRhs->data == NULL)
        return MATLIB_STATUS_UNINITIALIZED_MATRIX;

    if (pResult->width != pLhs->width
        ||
        pResult->height != pLhs->height
        ||
        pLhs->width != pRhs->width
        ||
        pLhs->height != pRhs->width)
        return MATLIB_STATUS_INCOMPATIBLE_SHAPE;

    if (pResult->dtype != pLhs->dtype || pLhs->dtype != pRhs->dtype)
        return MATLIB_STATUS_INCOMPATIBLE_DTYPE;

    size_t elemSize =
        pResult->elemSize,
        totalSize = pResult->width * pResult->height * pResult->elemSize;

    for (size_t offset = 0; offset < totalSize; offset += elemSize) {
        MATLIB_UNKOWN_OP(
            ((uint8_t*)pResult->data) + offset,
            ((uint8_t*)pLhs->data) + offset,
            ((uint8_t*)pRhs->data) + offset,
            -, pResult->dtype);
    }

    return MATLIB_STATUS_OK;
}

int matlib_scalar_mul(matrix_t* pMatrix, const void * const pScalar) {
    if (pMatrix->data == NULL)
        return MATLIB_STATUS_UNINITIALIZED_MATRIX;

    for (size_t i = 0; i < pMatrix->width * pMatrix->height; i++) {
        size_t offset = i * pMatrix->elemSize;
        MATLIB_UNKOWN_OP(
            ((uint8_t*)pMatrix->data)+offset,
            ((uint8_t*)pMatrix->data)+offset,
            pScalar, *, pMatrix->dtype);
    }

    return MATLIB_STATUS_OK;
}

int matlib_scalar_div(matrix_t* pMatrix, const void * const pScalar) {
    if (pMatrix->data == NULL)
        return MATLIB_STATUS_UNINITIALIZED_MATRIX;

    for (size_t i = 0; i < pMatrix->width * pMatrix->height; i++) {
        size_t offset = i * pMatrix->elemSize;
        MATLIB_UNKOWN_OP(
            ((uint8_t*)pMatrix->data)+offset,
            ((uint8_t*)pMatrix->data)+offset,
            pScalar, /, pMatrix->dtype);
    }

    return MATLIB_STATUS_OK;
}

int matlib_cover_column_row(
    matrix_t* pResult,
    const matrix_t * const pMatrix,
    size_t coveredRow, size_t coveredCol) {
    if (pMatrix->data == NULL || pResult->data == NULL)
        return MATLIB_STATUS_UNINITIALIZED_MATRIX;

    // non-square input
    if (pMatrix->width != pMatrix->height)
        return MATLIB_STATUS_INCOMPATIBLE_SHAPE;

    // bad result size
    if (pResult->width != pMatrix->width - 1
        ||
        pResult->height != pMatrix->height - 1)
        return MATLIB_STATUS_INCOMPATIBLE_SHAPE;

    if (pResult->dtype != pMatrix->dtype)
        return MATLIB_STATUS_INCOMPATIBLE_DTYPE;

    // out of bounds
    if (coveredRow >= pMatrix->height || coveredCol >= pMatrix->width)
        return MATLIB_STATUS_INDEX_OUT_OF_BOUNDS;

    // input is 1x1
    if (pMatrix->width == 1 && pMatrix->height == 1)
        return MATLIB_STATUS_INCOMPATIBLE_SHAPE;

    size_t resultIndex = 0, inputIndex = 0;
    size_t elemSize = pMatrix->elemSize;

    while (resultIndex < pResult->width * pResult->height) {
        size_t i, j;
        i = inputIndex / pMatrix->width;
        j = inputIndex - i * pMatrix->width;
        if (i != coveredRow && j != coveredCol) {
            size_t resultOffset = resultIndex * elemSize;
            size_t inputOffset = inputIndex * elemSize;
            memcpy(
                ((uint8_t*)pResult->data) + resultOffset,
                ((uint8_t*)pMatrix->data) + inputOffset,
                elemSize);
            resultIndex++;
        }
        inputIndex++;
    }

    return MATLIB_STATUS_OK;
}

int matlib_matrix_of_cofactors(
    matrix_t* pResult,
    const matrix_t * const pMatrix) {
    if (pResult->data == NULL || pMatrix->data == NULL)
        return MATLIB_STATUS_UNINITIALIZED_MATRIX;

    if (pMatrix->height != pMatrix->width)
        return MATLIB_STATUS_INCOMPATIBLE_SHAPE;

    if (pResult->width != pMatrix->width || pResult->height != pMatrix->height)
        return MATLIB_STATUS_INCOMPATIBLE_SHAPE;

    if (pResult->dtype != pMatrix->dtype)
        return MATLIB_STATUS_INCOMPATIBLE_DTYPE;

    size_t height = pMatrix->height, width = pMatrix->width;
    size_t elemSize = pMatrix->elemSize;
    uint8_t dtype = pMatrix->dtype;

    size_t i = 0;
    int8_t sign = 1;
    void * signcasted = malloc(elemSize);

    for (size_t row = 0; row < height; row++) {
        for (size_t col = 0; col < width; col++, i++, sign *= -1) {
            size_t offset = i * elemSize;
            matrix_t submatrix;
            matlib_matrix_create(&submatrix, dtype, height-1, width-1);
            matlib_cover_column_row(&submatrix, pMatrix, row, col);
            matlib_matrix_determinant(
                &submatrix,
                ((uint8_t*)pResult->data) + offset);
            MATLIB_TYPECAST(signcasted, sign, dtype);
            MATLIB_UNKOWN_OP(
                ((uint8_t*)pResult->data) + offset,
                ((uint8_t*)pResult->data) + offset,
                signcasted, *, dtype);
            matlib_matrix_free(&submatrix);
        }
        if (height % 2 == 0)  // this creates a 'checkerboard' sign:
            sign *= -1;
    }

    free(signcasted);

    return MATLIB_STATUS_OK;
}

int matlib_get_column(const matrix_t* const pMat, size_t col, void* dest) {
    if (pMat->data == NULL)
        return MATLIB_STATUS_UNINITIALIZED_MATRIX;

    if (col >= pMat->width)
        return MATLIB_STATUS_INDEX_OUT_OF_BOUNDS;

    for (size_t i = 0;
        i < pMat->height * pMat->elemSize;
        i += pMat->elemSize) {
        // this offset is used to get the corresponding value per-column
        size_t elementOffset = (i * pMat->width) + (col * pMat->elemSize);
        memcpy(
            (uint8_t*)(dest) + i,
            (uint8_t*)(pMat->data) + elementOffset,
            pMat->elemSize);
    }

    return MATLIB_STATUS_OK;
}
